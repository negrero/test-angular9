# Dashboard

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


Dashboard de información para COVID-19 

Objetivo

El objetivo de esta asignación es comprobar sus conocimientos de desarrollo Web, especialmente, con las herramientas de Angular 9.

Objetivos específicos

El dashboard deberá detectar de qué país se está conectando el usuario
Basado en el país desde el que se conecta el usuario, el dashboard mostrará la información de COVID disponible para ese país
Además, el dashboard deberá mostrar el lenguaje que se habla en el país desde el que se conecta, la moneda utilizada en dicho país, la capital 
El dashboard, deberá contener dos pedazos de información adicional:
Algún hecho aleatorio sobre Chuck Norris (referencia #2)
Algún hecho curioso relacionado con números (referencia #3)
Debe permitir cambiar de país para ver la información de otros países
El diseño (look and feel) es totalmente abierto

Entregables:

Se espera que usted entregue

Diseño de su solución
Consideraciones de seguridad, desempeño, etc
Asunciones/premisas en las que se ha basado para su implementación
Enlace a github donde está el código de su solución
Manuel de cómo ejecutar su solución


Sitios  WEB de referencia

https://covid19-api.weedmark.systems/
https://api.chucknorris.io/
http://numbersapi.com/#42
https://freegeoip.app/



# refs
## cors
* https://levelup.gitconnected.com/fixing-cors-errors-with-angular-cli-proxy-e5e0ef143f85
* https://daveceddia.com/access-control-allow-origin-cors-errors-in-angular/
* https://www.techiediaries.com/fix-cors-with-angular-cli-proxy-configuration/
## seguridad
las peticiones de https a http : 
* https://dev.to/mittalyashu/mixed-content-fetching-data-from-https-and-http-3n31
* https://web.dev/fixing-mixed-content/
* https://www.w3.org/TR/upgrade-insecure-requests/
* https://web.dev/what-is-mixed-content/
## Otras referencias
* http://country.io/data/
* https://restcountries.eu/#api-endpoints-list-of-codes