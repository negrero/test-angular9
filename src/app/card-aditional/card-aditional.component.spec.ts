import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardAditionalComponent } from './card-aditional.component';

describe('CardAditionalComponent', () => {
  let component: CardAditionalComponent;
  let fixture: ComponentFixture<CardAditionalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardAditionalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardAditionalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
