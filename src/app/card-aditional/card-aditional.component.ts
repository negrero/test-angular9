import { Component, Input, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Covid19ApiService} from '../service/covid19-api.service'

@Component({
  selector: 'app-card-aditional',
  templateUrl: './card-aditional.component.html',
  styleUrls: ['./card-aditional.component.css']
})
export class CardAditionalComponent implements OnInit {
  @Input()
  url: string;
  dataCovid: any;
  geoip: any;
  dataNumberapi: any;
  dataChucknorris: any;  
  currency;
  urlCovid = 'api/covid19';
  urlChucknorris = 'api/chucknorris';
  urlNumbersapi = 'api/numbersapi';
  urlGeoip = 'https://freegeoip.app/json/'
  urlCurrency = 'https://restcountries.eu/rest/v2/all'
  countrys: any;
  selectedOption;
  currentCountry: any;
  constructor(private http: HttpClient, private covid19Api: Covid19ApiService) { 
      
       this.loadCurrency(); 
  }

  ngOnInit(): void {  
    this.refreshChucknorris();
    this.refreshNumbersapi();        
    this.http.get<any>(this.urlGeoip, {responseType: 'json'}).subscribe(response => {
      this.geoip = response;
      this.selectedOption = this.geoip.country_code;      
      this.loadCurrency().subscribe(response => {
          this.countrys = response;
          let find = this.countrys.filter((e,i,a)=>{
            return e.alpha2Code === this.geoip.country_code;
          })                  
          let city = find[0]
          this.currentCountry = city;
          this.refreshCovid(this.geoip.country_code);
        })      
      
    }, err => {
      console.log(err)
    })
   
      
  }
  onChangeCountry(){
    this.refreshCovid(this.selectedOption);
    this.refreshNumbersapi();
    this.refreshChucknorris();
  }
  refreshCovid(code){
    let find = this.countrys.filter((e,i,a)=>{
      return e.alpha2Code === code;
    })                  
    let city = find[0]
    this.currentCountry = city;
    this.http.get(this.urlCovid + '?country=' + city.name, {responseType: 'json'}).subscribe(response => {      
      this.dataCovid = response;
    }, err => {
      console.log(err)
    })
  }
  refreshChucknorris(){
    this.http.get(this.urlChucknorris, {responseType: 'json'}).subscribe(response => {      
      this.dataChucknorris = response
    }, err => {
      console.log(err);          
    }); 
  }
  refreshNumbersapi(){
    this.http.get(this.urlNumbersapi, { responseType: 'text' }).subscribe(response_text => {
      this.dataNumberapi = response_text;
    }, err => {
      console.log(err);
    })    
  }
  loadCurrency(){
    return this.http.get<any>(this.urlCurrency, {responseType: 'json'})
  }
}

