import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  title = 'dashboard';

  apicovid19 = 'api/covid19';
  apichucknorris = 'api/chucknorris';
  apinumber = 'api/numbersapi';
}
