import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NumbersApiService {
  urlApi: string

  constructor() {
    this.urlApi = 'api/numbersapi';
   }
}
