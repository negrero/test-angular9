import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ChucknorrisApiService {
  urlApi
  constructor() {
    this.urlApi = 'api/chucknorris';
   }
   
}
