import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class Covid19ApiService {
  urlApi: string;
  constructor() {
    this.urlApi = 'api/covid19';
   }
}
