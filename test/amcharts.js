/**
 * ---------------------------------------
 * This demo was created using amCharts 4.
 *
 * For more information visit:
 * https://www.amcharts.com/
 *
 * Documentation is available at:
 * https://www.amcharts.com/docs/v4/
 * ---------------------------------------
 */
var data = [
  {
    city: null,
    province: "Andalusia",
    country: "Spain",
    lastUpdate: "2021-03-28T05:28:10+00:00",
    keyId: "Andalusia, Spain",
    confirmed: 501013,
    deaths: 9147,
    recovered: 10671,
  },
  {
    city: null,
    province: "Aragon",
    country: "Spain",
    lastUpdate: "2021-03-28T05:28:10+00:00",
    keyId: "Aragon, Spain",
    confirmed: 110163,
    deaths: 3372,
    recovered: 3772,
  },
  {
    city: null,
    province: "Asturias",
    country: "Spain",
    lastUpdate: "2021-03-28T05:28:10+00:00",
    keyId: "Asturias, Spain",
    confirmed: 46670,
    deaths: 1878,
    recovered: 1063,
  },
  {
    city: null,
    province: "Baleares",
    country: "Spain",
    lastUpdate: "2021-03-28T05:28:10+00:00",
    keyId: "Baleares, Spain",
    confirmed: 57315,
    deaths: 777,
    recovered: 1533,
  },
  {
    city: null,
    province: "C. Valenciana",
    country: "Spain",
    lastUpdate: "2021-03-28T05:28:10+00:00",
    keyId: "C. Valenciana, Spain",
    confirmed: 385049,
    deaths: 7076,
    recovered: 9970,
  },
  {
    city: null,
    province: "Canarias",
    country: "Spain",
    lastUpdate: "2021-03-28T05:28:10+00:00",
    keyId: "Canarias, Spain",
    confirmed: 46004,
    deaths: 663,
    recovered: 1537,
  },
  {
    city: null,
    province: "Cantabria",
    country: "Spain",
    lastUpdate: "2021-03-28T05:28:10+00:00",
    keyId: "Cantabria, Spain",
    confirmed: 25813,
    deaths: 541,
    recovered: 2287,
  },
  {
    city: null,
    province: "Castilla - La Mancha",
    country: "Spain",
    lastUpdate: "2021-03-28T05:28:10+00:00",
    keyId: "Castilla - La Mancha, Spain",
    confirmed: 174891,
    deaths: 5737,
    recovered: 6392,
  },
  {
    city: null,
    province: "Castilla y Leon",
    country: "Spain",
    lastUpdate: "2021-03-28T05:28:10+00:00",
    keyId: "Castilla y Leon, Spain",
    confirmed: 211261,
    deaths: 6608,
    recovered: 8716,
  },
  {
    city: null,
    province: "Catalonia",
    country: "Spain",
    lastUpdate: "2021-03-28T05:28:10+00:00",
    keyId: "Catalonia, Spain",
    confirmed: 529904,
    deaths: 13120,
    recovered: 26203,
  },
  {
    city: null,
    province: "Ceuta",
    country: "Spain",
    lastUpdate: "2021-03-28T05:28:10+00:00",
    keyId: "Ceuta, Spain",
    confirmed: 5023,
    deaths: 93,
    recovered: 163,
  },
  {
    city: null,
    province: "Extremadura",
    country: "Spain",
    lastUpdate: "2021-03-28T05:28:10+00:00",
    keyId: "Extremadura, Spain",
    confirmed: 70654,
    deaths: 1755,
    recovered: 2652,
  },
  {
    city: null,
    province: "Galicia",
    country: "Spain",
    lastUpdate: "2021-03-28T05:28:10+00:00",
    keyId: "Galicia, Spain",
    confirmed: 115836,
    deaths: 2314,
    recovered: 9204,
  },
  {
    city: null,
    province: "La Rioja",
    country: "Spain",
    lastUpdate: "2021-03-28T05:28:10+00:00",
    keyId: "La Rioja, Spain",
    confirmed: 27690,
    deaths: 743,
    recovered: 3107,
  },
  {
    city: null,
    province: "Madrid",
    country: "Spain",
    lastUpdate: "2021-03-28T05:28:10+00:00",
    keyId: "Madrid, Spain",
    confirmed: 618160,
    deaths: 14496,
    recovered: 40736,
  },
  {
    city: null,
    province: "Melilla",
    country: "Spain",
    lastUpdate: "2021-03-28T05:28:10+00:00",
    keyId: "Melilla, Spain",
    confirmed: 7726,
    deaths: 78,
    recovered: 125,
  },
  {
    city: null,
    province: "Murcia",
    country: "Spain",
    lastUpdate: "2021-03-28T05:28:10+00:00",
    keyId: "Murcia, Spain",
    confirmed: 108198,
    deaths: 1553,
    recovered: 2180,
  },
  {
    city: null,
    province: "Navarra",
    country: "Spain",
    lastUpdate: "2021-03-28T05:28:10+00:00",
    keyId: "Navarra, Spain",
    confirmed: 53517,
    deaths: 1117,
    recovered: 3905,
  },
  {
    city: null,
    province: "Pais Vasco",
    country: "Spain",
    lastUpdate: "2021-03-28T05:28:10+00:00",
    keyId: "Pais Vasco, Spain",
    confirmed: 160437,
    deaths: 3942,
    recovered: 16160,
  },
  {
    city: null,
    province: "Unknown",
    country: "Spain",
    lastUpdate: "2021-03-28T05:28:10+00:00",
    keyId: "Unknown, Spain",
    confirmed: 0,
    deaths: 0,
    recovered: null,
  },
];
/**
 * ---------------------------------------
 * This demo was created using amCharts 4.
 *
 * For more information visit:
 * https://www.amcharts.com/
 *
 * Documentation is available at:
 * https://www.amcharts.com/docs/v4/
 * ---------------------------------------
 */

// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
var chart = am4core.create("chartdiv", am4charts.XYChart);

// Add data
chart.data = [
  {
    year: "2003",
    europe: 2.5,
    namerica: 2.5,
    asia: 2.1,
    lamerica: 1.2,
    meast: 0.2,
    africa: 0.1,
  },
  {
    year: "2004",
    europe: 2.6,
    namerica: 2.7,
    asia: 2.2,
    lamerica: 1.3,
    meast: 0.3,
    africa: 0.1,
  },
  {
    year: "2005",
    europe: 2.8,
    namerica: 2.9,
    asia: 2.4,
    lamerica: 1.4,
    meast: 0.3,
    africa: 0.1,
  },
];
chart.data = data;
// Create axes
var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
categoryAxis.dataFields.category = "province";
categoryAxis.title.text = "Local country offices";
categoryAxis.renderer.grid.template.location = 0;
categoryAxis.renderer.minGridDistance = 20;
categoryAxis.renderer.cellStartLocation = 0.1;
categoryAxis.renderer.cellEndLocation = 0.9;
categoryAxis.renderer.labels.template.rotation = 90;

var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
valueAxis.min = 0;
valueAxis.title.text = "Expenditure (M)";
// axis break
// Create value axis break
var axisBreak = valueAxis.axisBreaks.create();
axisBreak.startValue = 9000;
axisBreak.endValue = 600000;
axisBreak.breakSize = 0.01;

// Add axis break events
axisBreak.events.on("over", () => {
  axisBreak.animate(
    [{ property: "breakSize", to: 1 }, { property: "opacity", to: 0.1 }],
    1500,
    am4core.ease.sinOut
  );
});
axisBreak.events.on("out", () => {
  axisBreak.animate(
    [{ property: "breakSize", to: 0.01 }, { property: "opacity", to: 1 }],
    1000,
    am4core.ease.quadOut
  );
});
// Create series
function createSeries(field, name, stacked) {
  var series = chart.series.push(new am4charts.ColumnSeries());
  series.dataFields.valueY = field;
  series.dataFields.categoryX = "province";
  series.name = name;
  series.columns.template.tooltipText = "{name}: [bold]{valueY}[/]";
  series.stacked = stacked;
  series.columns.template.width = am4core.percent(95);
}

//createSeries("europe", "Europe", false);
//createSeries("namerica", "North America", true);
//createSeries("asia", "Asia", false);
//confirmed: 0,
//deaths: 0,
//recovered: null,
createSeries("confirmed", "confirmed", false);
createSeries("deaths", "deaths", false);
createSeries("recovered", "recovered", false);

// Add legend
chart.legend = new am4charts.Legend();
chart.scrollbarX = new am4core.Scrollbar();
chart.scrollbarY = new am4core.Scrollbar();
